git b91f96936098a10ccb40ffbf507567e18c5191bd

---

# Настройка

- [Вступление](#introduction)
- [Настройки среды](#environment-configuration)
- [Настройка сервис-провайдеров](#provider-configuration)
- [Защита конфиденциальных данных](#protecting-sensitive-configuration)
- [Режим обслуживания](#maintenance-mode)

<a name="introduction"></a>
## Вступление

Все файлы настроек Laravel хранятся в папке `config`. Каждая настройка в каждом файле задокументирована, поэтому не стесняйтесь изучить эти файлы и познакомиться с различными настройками, доступными для вас.

Иногда вам нужно получить значение настройки во время работы приложения. Это можно сделать, используя класс `Config`.

#### Получение значения настройки

	Config::get('app.timezone');

Вы также можете указать значение по умолчанию, которое будет возвращено, если настройка не существует:

	$timezone = Config::get('app.timezone', 'UTC');

#### Изменение значения во время выполнения

Заметьте, что синтаксис с точкой может использоваться для доступа к разным файлам настроек. Вы можете задать значение во время выполнения:

	Config::set('database.default', 'sqlite');

Значения, установленные таким образом, сохраняются только для текущего запроса и не влияют на более поздние запросы.

<a name="environment-configuration"></a>
## Настройки среды (the environment)

Часто необходимо иметь разные значения для разных настроек в зависимости от среды, в которой выполняется приложение. Например, вы можете захотеть использовать разные драйвера кэша на локальном и производственном серверах. Это легко достигается использованием настроек, зависящих от среды.

Просто создайте новую папку внутри папки `config`, название которой совпадает с именем вашей среды, таким как `local`. Затем создайте файлы настроек, которые вы хотите переопределить, и укажите в них значения для этой среды. Например, вы можете переопределить драйвер кэша для локальной среды, создав файл `cache.php` внутри `config/local` с таким содержимым:

	<?php

	return array(

		'driver' => 'file',

	);

> **Примечание:** Не используйте слово 'testing' для названия среды - оно зарезервировано для юнит-тестов.

Заметьте, что вам не нужно указывать _каждую_ настройку, которая есть в исходном конфигурационном файле, кроме настроек, которые вы хотите переопределить. Настройки среды будут наложены на эти базовые файлы.

Теперь нам нужно сообщить Laravel как определить, в какой среде он работает. Начальная среда всегда `production`. Однако, вы можете настроить и другие среды в файле `bootstrap/environment.php` который находится в корне установки Laravel. В этом файле вы найдёте вызов метода `$app->detectEnvironment`. Массив, который ему передаётся, используется для определения текущей среды. Вы можете добавить в него другие среды и имена компьютеров при необходимости.

	<?php

	$env = $app->detectEnvironment(array(

		'local' => array('your-machine-name'),

	));

В этом примере 'local' это название среды, а 'your-machine-name' - имя вашего компьютера. Чтобы узнать его, в Mac OS или Linux вы можете использовать команду hostname в терминале, а в Windows - зайти в Панель управления - Система.

Если вам нужно более гибкое определение среды, вы можете передать в метод `detectEnvironment` функцию-замыкание (Сlosure), позволяющую вам реализовать определение среды так, как нужно именно вам:

	$env = $app->detectEnvironment(function()
	{
		return $_SERVER['MY_LARAVEL_ENV'];
	});

#### Получение текущей среды

Вы можете получить текущую среду с помощью метода `environment`:

	$environment = App::environment();

Вы также можете передать аргументы в этот метод чтобы проверить, совпадает ли среда с переданным значением:

	if (App::environment('local'))
	{
		// Среда - local
	}

	if (App::environment('local', 'staging'))
	{
		// Среда - local ИЛИ staging
	}

<a name="provider-configuration"></a>
## Настройка сервис-провайдеров

При использовании среды, вы возможно захотите "добавить" дополнительные [сервис-провайдеры](/docs/ioc#service-providers) к исходным в файле `app`. Однако, если вы это сделаете, вы заметите, что сервис-провайдеры среды переопределяют сервис-провайдеры в вашем исходном файле настроек `app`. Чтобы заставить сервис-провайдеры не переопределять, а добавляться к существующим, используйте вспомогательный метод `append_config` в вашем файле настроек среды `app`:

	'providers' => append_config(array(
		'LocalOnlyServiceProvider',
	)) 


<a name="protecting-sensitive-configuration"></a>
## Защита конфиденциальных данных

Для "настоящих" приложений предпочтительно хранить конфиденциальные данные вне ваших обычных файлов настроек. Такие данные как пароли к базам данных, ключи доступа к различным API-сервисам, ключи шифрования должны храниться вне ваших обычных файлов настроек всегда, когда это только возможно. И где же мы должны их хранить в таком случае? К счастью, Laravel предоставляет очень простое решение для защиты таких настроек - хранение их в "dot" файлах (имена этих файлов начинаются с точки).

Во-первых, [настройте ваше приложение](/docs/configuration#environment-configuration) таким образом, чтобы ваш компьютер определялся в среде `local`. Далее, создайте файл `.env.local.php` в корневой папке вашего проекта (в ней обычно находится файл `composer.json`). Файл `.env.local.php` должен возвращать массив значений, очень похожий на стандартный файл настроек Laravel:

	<?php

	return array(

		'TEST_STRIPE_KEY' => 'super-secret-sauce',

	);

Все эти значения будут автоматически доступны в вашем приложении в суперглобальных переменных `$_ENV` и `$_SERVER`. Теперь вы можете обратиться к этим глобальным переменным внутри ваших файлов настроек:

	'key' => $_ENV['TEST_STRIPE_KEY']

Убедитесь, что вы добавили файл `.env.local.php` в ваш файл `.gitignore`. Это позволит другим разработчикам в вашей команде создавать свои собственные локальные файлы настроек, также как и спрятать ваши конфиденциальные данные от системы котроля.

Теперь, на вашем производственном сервере, создайте файл `.env.php` в корне вашего приложения, который будет содержать соответствующие значения для среды производства. Как и файл `.env.local.php`, производственный файл `.env.php` никогда не должен включаться в систему контроля.

> **Примечание:** Вы можете создавать файлы для каждой среды вашего приложения. Например, среда `development` будет всегда загружать файл `.env.development.php`, если он существует. Однако, среда `production` (среда производства) всегда использует файл `.env.php`.

<a name="maintenance-mode"></a>
## Режим обслуживания

Когда ваше приложение находится в режиме обслуживания (maintenance mode), специальный шаблон будет отображаться вместо всех ваших маршрутов. Это позволяет "отключать" ваше приложение во время обновления или обслуживания. Проверка на режим обслуживания уже включена в стандартный фильтр `App::before` в файле `app/Http/Filters/MaintenanceFilter.php`. Ответ от этой проверки будет возвращен пользователю когда приложение находится в режиме обслуживания.

Для включения этого режима просто выполните команду Artisan-а `down`:

	php artisan down

Чтобы выйти из режима обслуживания выполните команду `up`:

	php artisan up

### Режим обслуживания и очереди

Пока ваше приложение находится в режиме обслуживания, [очереди](/docs/queues) не будут обрабатываться. Работа очередей будет возобновлена когда приложение выйдет из режима обслуживания.
